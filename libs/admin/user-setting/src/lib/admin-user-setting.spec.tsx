import { render } from '@testing-library/react';

import AdminUserSetting from './admin-user-setting';

describe('AdminUserSetting', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AdminUserSetting />);
    expect(baseElement).toBeTruthy();
  });
});
