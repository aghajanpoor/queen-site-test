import { Box, Button, Dialog, Grid, Typography } from '@mui/material';
import styles from './admin-user-setting-list.module.scss';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import AddAdminUserSetting from '../add-admin-user-setting/add-admin-user-setting';
import { useEffect, useState } from 'react';
import RemoveAdminUserSetting from '../remove-admin-user-setting-dialog/remove-admin-user-setting-dialog';
import { useTranslation } from 'react-i18next';
import useDeleteApi from '../../../../../utils/src/lib/api-hook/delete-request/useDeleteApi';
import useFetchById from '../../../../../utils/src/lib/api-hook/fetch-request-by-id/useFetchById';
import Loading from '../../../../../utils/src/lib/loading/Loading';
import Icons from '../../../../../utils/src/lib/icons/Icons';
import { UserSettingModel } from '@test-site/utils';

/* eslint-disable-next-line */
export interface AdminUserSettingListProps {
  item: UserSettingModel;
  refetch: () => void;
  onModify: (data: UserSettingModel) => void;
}

export function AdminUserSettingList(props: AdminUserSettingListProps) {
  const { t } = useTranslation(['user', 'common']);
  const { item, refetch, onModify } = props;
  const [selectedItemId, setSelectedItemId] = useState<number>(-1);
  const [userSettings, setUserSettings] = useState<UserSettingModel>();
  const [openDeleteState, setOpenDeleteState] = useState<boolean>(false);
  const { deleted, loading, deleteApi } = useDeleteApi('/items');
  const { response, fetchById } = useFetchById('/items');

  useEffect(() => {    
    setSelectedItemId(-1);
    setOpenDeleteState(false);
    refetch();
  }, [deleted]);

  useEffect(() => {
    if (response) {
      setUserSettings(response);
    }
  }, [response]);

  return (
    <>
      {loading && <Loading />}
      <Dialog
        aria-labelledby="settings-panel"
        aria-describedby="settings"
        open={openDeleteState}
        onClose={() => setOpenDeleteState(false)}
        BackdropProps={{ invisible: false }}
      >
        <RemoveAdminUserSetting
          onCancel={() => setOpenDeleteState(false)}
          onDelete={() => deleteApi(selectedItemId)}
        />
      </Dialog>

      <Grid
        container
        className={styles['container']}
        sx={{ backgroundColor: (theme: any) => theme.custom.lightGray }}
      >
        <Grid item lg={9} container alignItems="center">
          <Box
            sx={{
              margin: '0 5px',
            }}
          >
            <Icons icon={item.icon} />
          </Box>
          <Typography color="secondary" component="span" fontSize={14} mr={1} ml={1}>
            {t(`common:${item.text}`)}
          </Typography>
          <Typography color="secondary" component="span" fontSize={12} mr={1} ml={1}>
            {t('link')}:{' '}
            <Typography color="primary" component="span" fontSize={14}>
              {item.linkUrl}
            </Typography>
          </Typography>
        </Grid>
        <Grid item lg={3}>
          <Button
            variant="text"
            color="primary"
            onClick={() => fetchById(item.id)}
          >
            <EditIcon fontSize="small" />
            {t('common:edit')}
          </Button>
          <Button
            variant="text"
            color="warning"
            onClick={() => {
              setSelectedItemId(item.id);
              setOpenDeleteState(true);
            }}
          >
            <DeleteIcon fontSize="small" />
            {t('common:delete')}
          </Button>
        </Grid>
        {userSettings && (
          <Grid item lg={12}>
            <div className={styles['collapse__content']}>
              {/* EDIT USER SETTING */}
              <AddAdminUserSetting
                onClose={() => {
                  setSelectedItemId(-1);
                  setUserSettings(undefined);
                }}
                onModify={(data: any) => {
                  onModify(data);
                  setSelectedItemId(-1);
                  setUserSettings(undefined);
                }}
                model={userSettings}
                title={t('updateConnectionPath')}
                btnLabel={t('editConnectionPath')}
              />
            </div>
          </Grid>
        )}
      </Grid>
    </>
  );
}

export default AdminUserSettingList;
