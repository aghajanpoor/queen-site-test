import { Button, Grid, Typography } from '@mui/material';
import { useTranslation } from 'react-i18next';

/* eslint-disable-next-line */
export interface RemoveAdminUserSettingProps {
  onCancel: () => void;
  onDelete: () => void;
}

export function RemoveAdminUserSetting(props: RemoveAdminUserSettingProps) {
  const { onCancel, onDelete } = props;
  const { t } = useTranslation(['user', 'common']);
  
  return (
    <Grid container>
      <Typography color="secondary" component="p" fontSize={15} mb={2}>
        {t("common:areYouSureOfYourDecision")}
      </Typography>
      <Grid item lg={12} container justifyContent="flex-end">
        <Button variant="contained" color="primary" type="submit" onClick={() => onDelete()}>
        {t("common:delete")}
        </Button>
        <Button
          variant="outlined"
          color="primary"
          type="button"
          onClick={() => onCancel()}
        >
          {t("common:cancel")}
        </Button>
      </Grid>
    </Grid>
  );
}

export default RemoveAdminUserSetting;
