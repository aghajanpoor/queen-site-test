import AddAdminUserSetting from './add-admin-user-setting/add-admin-user-setting';
import AdminUserSettingList from './admin-user-setting-list/admin-user-setting-list';
import styles from './admin-user-setting.module.scss';
import AddIcon from '@mui/icons-material/Add';
import { Box, Grid, IconButton, TextField, Typography } from '@mui/material';
import { useContext, useEffect, useRef, useState } from 'react';
import SearchIcon from '@mui/icons-material/Search';
import { useTranslation } from 'react-i18next';
import useFetch from '../../../../utils/src/lib/api-hook/fetch-request/useFetch';
import useCreateApi from '../../../../utils/src/lib/api-hook/create-request/useCreateApi';
import useUpdateApi from '../../../../utils/src/lib/api-hook/update-request/useUpdateApi';
import Loading from '../../../../utils/src/lib/loading/Loading';
import { SnackContext, UserSettingModel } from '@test-site/utils';

/* eslint-disable-next-line */
export interface AdminUserSettingProps {}

export function AdminUserSetting(props: AdminUserSettingProps) {
  const { t } = useTranslation(['user', 'common']);
  const addBtnRef = useRef<HTMLInputElement>(null);
  const [searchValue, setSearchValue] = useState<string>('');
  const { response, loading, refetch } = useFetch('/items');
  const {
    loading: createLoading,
    callApi,
    refetchList,
  } = useCreateApi('/items');
  const { loading: updateLoading, updateApi } = useUpdateApi('/items');
  const { showAlert } = useContext(SnackContext);

  useEffect(() => {
    if (refetchList) {
      refetch({});
    }
  }, [refetchList]);

  const onModify = (data: UserSettingModel) => {
    if (data.id) {
      updateApi(data);
      refetch({});
    } else {
      let addedIndex = -1;
      if (response && response.length !== 0) {
        addedIndex = response?.findIndex(
          (item) => item?.typeId === data.typeId
        );
      }

      if (addedIndex === -1) {
        data.id = response ? response.length + 100 : 1;
        callApi(data);
        addBtnRef.current?.click();
        refetch({});
      } else {
        showAlert({
          type: 'error',
          message: t('common:thisItemAddedBefore'),
        });
      }
    }
  };

  return (
    <>
      {(loading || createLoading || updateLoading) && <Loading />}
      <Box
        className={styles['container']}
        sx={{ backgroundColor: (theme: any) => theme.custom.gray }}
      >
        <Typography color="secondary" component="h6" fontSize={12}>
          {t('connectionRoute')}
        </Typography>

        <div className={styles['collapse']}>
          <div className={styles['collapse__item']}>
            <input
              type="checkbox"
              id="collapse-1"
              className={styles['collapse__checkbox']}
              ref={addBtnRef}
            />
            <label htmlFor="collapse-1" className={styles['collapse__header']}>
              <AddIcon color="primary" />
              <Typography color="primary" component="span" fontSize={12}>
                {t('addConnectionPath')}
              </Typography>
            </label>
            <Box
              className={styles['collapse__content']}
              sx={{ backgroundColor: (theme: any) => theme.custom.lightGray }}
            >
              {/* CREATE USER SETTING */}
              <AddAdminUserSetting
                onClose={() => addBtnRef.current?.click()}
                onModify={(data: UserSettingModel) => onModify(data)}
                title={t('addConnectionPath')}
                btnLabel={t('registerConnectionPath')}
              />
            </Box>
          </div>
        </div>

        {response?.map((item) => (
          <AdminUserSettingList
            key={item.id}
            item={item}
            refetch={() => refetch({})}
            onModify={(data: any) => onModify(data)}
          />
        ))}
      </Box>
    </>
  );
}

export default AdminUserSetting;
