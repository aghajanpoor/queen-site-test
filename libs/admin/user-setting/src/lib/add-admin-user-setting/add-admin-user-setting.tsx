import { Button, Grid, Typography } from '@mui/material';
import styles from './add-admin-user-setting.module.scss';
import { useForm } from 'react-hook-form';
import SelectInput from '../../../../../utils/src/lib/select-input/SelectInput';
import TextInput from '../../../../../utils/src/lib/text-input/TextInput';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useTranslation } from 'react-i18next';
import { UserSettingModel } from '@test-site/utils';

/* eslint-disable-next-line */
export interface AddAdminUserSettingProps {
  onClose: () => void;
  onModify: (data: UserSettingModel) => void;
  model?: UserSettingModel;
  title: string;
  btnLabel: string;
}

const CommunicationsType = [
  {
    text: 'twitter',
    typeId: 'TWITTER',
    icon: 'twitter',
  },
  {
    text: 'insta',
    typeId: 'INSTAGRAM',
    icon: 'insta',
  },
  {
    text: 'facebook',
    typeId: 'FACEBOOK',
    icon: 'facebook',
  },
  {
    text: 'telegram',
    typeId: 'TELEGRAM',
    icon: 'telegram',
  },
  {
    text: 'linkedin',
    typeId: 'LINKEDIN',
    icon: 'linkedin',
  },
  {
    text: 'web',
    typeId: 'WEB',
    icon: 'web',
  },
];

export function AddAdminUserSetting(props: AddAdminUserSettingProps) {
  const { t } = useTranslation(['user', 'common']);
  const { onClose, onModify, model, title, btnLabel } = props;

  const schema = yup.object().shape({
    typeId: yup.string().required(t('common:isRequered')),
    linkUrl: yup
      .string()
      .required(t('common:isRequered'))
      .matches(
        new RegExp('^(https?)://[^s$.?#].[^s]*$'),
        t('common:isNotValid')
      ),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
    setValue,
  } = useForm<any>({
    mode: 'onBlur',
    resolver: yupResolver(schema),
    defaultValues: {
      id: model?.id ?? undefined,
      typeId: model?.typeId ?? undefined,
      text: model?.text ?? '',
      linkUrl: model?.linkUrl ?? '',
      icon: model?.icon ?? '',
    },
  });

  const onSubmit = (value: any) => {
    onModify(value);
    reset();
  };

  return (
    <div className={styles['container']}>
      <Typography color="secondary" component="span" fontSize={13}>
        {title}
      </Typography>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={1} rowSpacing={2} alignItems="center">
          <Grid item lg={4} style={{textAlign: 'right'}}>
            <SelectInput
              control={control}
              name="typeId"
              placeholder={t('type')}
              label=""
              options={CommunicationsType}
              error={!!errors['typeId']}
              helpText={errors['typeId']?.message}
              onChangeFunction={(v) => {
                setValue('text', v.text);
                setValue('icon', v.icon);
              }}
            />
          </Grid>
          <Grid item lg={8}>
            <TextInput
              control={control}
              name="linkUrl"
              placeholder={t('link')}
              label=""
              error={!!errors['linkUrl']}
              helpText={errors['linkUrl']?.message}
            />
          </Grid>
          <Grid item lg={12} container justifyContent="flex-end">
            <Button variant="contained" color="primary" type="submit">
              {btnLabel}
            </Button>
            <Button
              variant="outlined"
              color="primary"
              type="button"
              onClick={() => {
                reset();
                onClose();
              }}
            >
              {t('common:cancel')}
            </Button>
          </Grid>
        </Grid>
      </form>
    </div>
  );
}

export default AddAdminUserSetting;

