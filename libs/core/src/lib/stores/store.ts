import { configureStore } from "@reduxjs/toolkit";
import themeReducer from "./themeSlice";
import langReducer from "./langSlice";
import { useDispatch } from "react-redux";

const store = configureStore({
  reducer: {
    theme: themeReducer,
    lang: langReducer,
  },
});
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch: () => AppDispatch = useDispatch; // Export a hook that can be reused to resolve types

export default store;
