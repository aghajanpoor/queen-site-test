import createGenericSlice from "./GenericSlice";
import { PayloadAction } from "@reduxjs/toolkit";

export interface GenericState<T> {
  data?: T;
  status: "loading" | "finished" | "error";
}

export const slice = createGenericSlice({
  name: "theme",
  initialState: {
    status: "loading",
    data: {
      name: "darkTheme",
    },
  } as GenericState<any>,
  reducers: {
    changeTheme: (state: any, action: PayloadAction<any>) => {
      state.status = "finished";
      state.data = action.payload;
    },
  },
});
export const selectTheme = (state: { theme: { data: any } }) =>
  state.theme.data.name;
export const { changeTheme } = slice.actions;

export default slice.reducer;
