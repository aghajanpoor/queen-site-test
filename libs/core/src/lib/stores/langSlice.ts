import createGenericSlice from "./GenericSlice";
import { PayloadAction } from "@reduxjs/toolkit";

export interface GenericState<T> {
  data?: T;
  status: "loading" | "finished" | "error";
}

export const slice = createGenericSlice({
  name: "lang",
  initialState: {
    status: "loading",
    data: {
      language: "fa",
    },
  } as GenericState<any>,
  reducers: {
    changeLang: (state: any, action: PayloadAction<any>) => {
      state.status = "finished";
      state.data = action.payload;
    },
  },
});
export const selectLang = (state: { lang: { data: any } }) =>
  state.lang.data.language;
export const { changeLang } = slice.actions;

export default slice.reducer;
