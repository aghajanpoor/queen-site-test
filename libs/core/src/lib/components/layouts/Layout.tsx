import { Grid, Paper } from '@mui/material';
import { FC } from 'react';
import { Switch } from 'react-router';
import Navbar from '../navbar/Navbar';
import { PrivateRoute } from '../routes/PrivateRoute';
import styles from './layout.module.scss';

interface IProps {
  menuItems: any[];
}
const Layout: FC<IProps> = ({ menuItems }) => {
  return (
    <Grid
      container
      alignContent="flex-start"
      component={Paper}
      className={styles['main']}
    >
      <Grid className={styles['main-content']}>
        <Navbar />

        <Grid item xs={12} container spacing={1} alignContent="flex-start">
          <Switch>
            {menuItems.map((item) => (
              <PrivateRoute
                key={item.path}
                path={item.path}
                exact
                component={item.component}
              />
            ))}
          </Switch>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Layout;
