import { FC } from 'react';
import { Redirect, Route } from 'react-router-dom';

interface IProps {
  component: any;
  path: string;
  exact?: true;
}
export const PrivateRoute: FC<IProps> = ({ component, path, exact }) => {
  return <Route path={path} component={component} exact={exact} />;
};
