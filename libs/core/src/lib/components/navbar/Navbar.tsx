import {
  Breadcrumbs,
  Button,
  Grid,
  IconButton,
  Typography,
  Link,
} from '@mui/material';
import styles from './navbar.module.scss';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import { useDispatch, useSelector } from 'react-redux';
import { selectTheme, changeTheme } from '../../stores/themeSlice';
import { selectLang, changeLang } from '../../stores/langSlice';
import LightModeIcon from '@mui/icons-material/LightMode';
import { useTranslation } from 'react-i18next';

const Navbar = () => {
  const dispatch = useDispatch();
  const theme = useSelector(selectTheme);
  const lang = useSelector(selectLang);
  const { t } = useTranslation(['user', 'common']);

  const themeHandler = () => {
    if (theme === 'darkTheme') {
      dispatch(changeTheme({ name: 'lightTheme' }));
    } else if (theme === 'lightTheme') {
      dispatch(changeTheme({ name: 'darkTheme' }));
    }
  };

  const onChangeLang = (lang: string) => {
    dispatch(changeLang({ language: lang }));
  };

  const breadcrumbs = [
    <Link key="1" color="inherit">
      {t('common:home')}
    </Link>,
    <Link
      key="2"
      color="inherit"
    >
      {t('common:user')}
    </Link>,
    <Typography key="3" color="text.primary">
      {t('common:userSetting')}
    </Typography>,
  ];

  return (
    <Grid container className={styles['navbar']}>
      <Grid item lg={2}>
        <Typography color="secondary" component="h2" fontSize={20}>
          <b>{t('common:userSetting')}</b>
        </Typography>
      </Grid>
      <Grid item lg={10} container justifyContent="flex-end">
        <Button
          variant="text"
          color={lang === 'en' ? 'primary' : 'secondary'}
          onClick={() => onChangeLang('en')}
        >
          <Typography fontSize={12}>ENGLISH</Typography>
        </Button>
        <Button
          variant="text"
          color={lang === 'fa' ? 'primary' : 'secondary'}
          onClick={() => onChangeLang('fa')}
        >
          <Typography fontSize={12}>فارسی</Typography>
        </Button>
        <IconButton color="secondary" onClick={() => themeHandler()}>
          {theme === 'darkTheme' && <DarkModeIcon />}
          {theme === 'lightTheme' && <LightModeIcon color="primary" />}
        </IconButton>
      </Grid>
      <Grid item lg={12} mt={1}>
        <Breadcrumbs separator="●" aria-label="breadcrumb">
          {breadcrumbs}
        </Breadcrumbs>
      </Grid>
    </Grid>
  );
};

export default Navbar;
