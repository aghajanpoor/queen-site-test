import { createTheme } from '@mui/material';

export const darkTheme = createTheme({
  direction: 'rtl',
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          fontFamily: 'iranSans',
          fontSize: 14,
        },
        containedPrimary: {
          height: 30,
          borderRadius: '4px',
          color: 'rgba(0, 0, 0)',
          backgroundColor: 'rgb(255, 168, 46)',
          boxShadow:
            'rgb(0 0 0 / 20%) 0px 3px 1px -2px, rgb(0 0 0 / 14%) 0px 2px 2px 0px, rgb(0 0 0 / 12%) 0px 1px 5px 0px',
          fontFamily: 'iranSans',
          fontSize: 13,
          fontWeight: 500,
          minWidth: 64,
          margin: 2,
          padding: '4px 10px',
        },
        outlinedPrimary: {
          height: 30,
          borderRadius: '4px',
          border: '1px solid rgba(255, 168, 46, 0.5)',
          color: 'rgb(255, 168, 46)',
          backgroundColor: 'transparent',
          fontFamily: 'iranSans',
          fontSize: 13,
          fontWeight: 500,
          minWidth: 64,
          margin: 2,
          padding: '4px 10px',
        },
        textPrimary: {
          fontFamily: 'iranSans',
          color: 'rgb(255, 168, 46)',
        },
        textWarning: {
          fontFamily: 'iranSans',
          color: 'rgb(244, 67, 54)',
        },
      },
    },
    MuiDialog: {
      styleOverrides: {
        paper: {
          overflow: 'visible',
          backgroundColor: 'rgb(33, 43, 53)',
          padding: '40px',
          borderRadius: 9,
          color: '#fff',
        },
      },
    },
    MuiInputBase: {
      styleOverrides: {
        root: {
          fontFamily: 'iranSans',
          color: '#fff',
          ':hover': {
            '.MuiOutlinedInput-notchedOutline': {
              borderColor: '#fff !important',
            },
          },
        },
      },
    },
    MuiFormLabel: {
      styleOverrides: {
        root: {
          color: '#7E848B',
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        notchedOutline: {
          borderColor: '#6B7279',
        },
      },
    },
    MuiAutocomplete: {
      styleOverrides: {
        root: {
          fontFamily: 'iranSans',
        },
        popupIndicator: {
          color: 'rgb(255 255 255 / 80%)',
        },
        option: {
          color: '#fff',
          ':hover': {
            backgroundColor: '#343D48',
          },
        },
      },
    },
    MuiInputLabel: {
      styleOverrides: {
        root: {
          fontFamily: 'iranSans',
        },
      },
    },
    MuiTypography: {
      styleOverrides: {
        root: {
          fontFamily: 'iranSans',
        },
      },
    },
    MuiBreadcrumbs: {
      styleOverrides: {
        li: {
          color: 'rgb(121, 131, 142)',
          fontSize: 14,
          p: {
            color: 'rgb(121, 131, 142)',
            fontSize: 15,
          },
        },
        separator: {
          color: 'rgb(121, 131, 142)',
          fontSize: 20,
        },
      },
    },
  },
  palette: {
    primary: {
      main: '#ffa82e',
    },
    secondary: {
      main: '#fff',
    },
    background: {
      paper: '#161b25',
    },
  },
  custom: {
    gray: 'rgb(33, 43, 53)',
    lightGray: 'rgb(52, 61, 72)',
  },
});
