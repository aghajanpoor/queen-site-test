import { createTheme } from '@mui/material';

export const lightTheme = createTheme({
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          fontFamily: 'iranSans',
          fontSize: 14,
        },
        containedPrimary: {
          height: 30,
          borderRadius: '4px',
          color: 'rgba(0, 0, 0)',
          backgroundColor: 'rgb(255, 168, 46)',
          boxShadow:
            'rgb(0 0 0 / 20%) 0px 3px 1px -2px, rgb(0 0 0 / 14%) 0px 2px 2px 0px, rgb(0 0 0 / 12%) 0px 1px 5px 0px',
          fontFamily: 'iranSans',
          fontSize: 13,
          fontWeight: 500,
          minWidth: 64,
          margin: 2,
          padding: '4px 10px',
        },
        outlinedPrimary: {
          height: 30,
          borderRadius: '4px',
          border: '1px solid rgba(255, 168, 46, 0.5)',
          color: 'rgb(255, 168, 46)',
          backgroundColor: 'transparent',
          fontFamily: 'iranSans',
          fontSize: 13,
          fontWeight: 500,
          minWidth: 64,
          margin: 2,
          padding: '4px 10px',
        },
        textPrimary: {
          fontFamily: 'iranSans',
          color: 'rgb(255, 168, 46)',
        },
        textWarning: {
          fontFamily: 'iranSans',
          color: 'rgb(244, 67, 54)',
        },
      },
    },
    MuiDialog: {
      styleOverrides: {
        paper: {
          overflow: 'visible',
          backgroundColor: 'rgb(252, 255, 255)',
          padding: '40px',
          borderRadius: 9,
          color: '#000',
        },
      },
    },
    MuiInputBase: {
      styleOverrides: {
        root: {
          fontFamily: 'iranSans',
          color: '#000',
        },
      },
    },
    MuiFormLabel: {
      styleOverrides: {
        root: {
          color: '#000',
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        notchedOutline: {
          borderColor: '#000',
        },
      },
    },
    MuiAutocomplete: {
      styleOverrides: {
        root: {
          fontFamily: 'iranSans',
        },
        popupIndicator: {
          color: 'rgb(0 0 0 / 80%)',
        },
      },
    },
    MuiInputLabel: {
      styleOverrides: {
        root: {
          fontFamily: 'iranSans',
        },
      },
    },
    MuiTypography: {
      styleOverrides: {
        root: {
          fontFamily: 'iranSans',
        },
      },
    },
  },
  palette: {
    primary: {
      main: '#ffa82e',
    },
    secondary: {
      main: '#000',
    },
    background: {
      paper: '#fff',
    },
  },
  custom: {
    gray: '#fff',
    lightGray: '#fff',
  },
});
