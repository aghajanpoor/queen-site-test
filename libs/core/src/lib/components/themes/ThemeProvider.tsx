import React from 'react';
import { ThemeProvider as MuiThemeProvider } from '@mui/material/styles';
import { getThemeByName } from './base';
import { useSelector } from 'react-redux';
import { selectTheme } from '../../stores/themeSlice';
import { CacheProvider } from '@emotion/react';
import createCache from '@emotion/cache';
import rtlPlugin from 'stylis-plugin-rtl';
import { prefixer } from 'stylis';
import { selectLang } from '../../stores/langSlice';

const ThemeProvider: React.FC<any> = ({ children }) => {
  const themeName = useSelector(selectTheme);
  const theme = getThemeByName(themeName);
  const lang = useSelector(selectLang);

  const stylisPlugins = [prefixer];
  if (lang === 'fa') {
    stylisPlugins.push(rtlPlugin);
  }
  const cacheDir = createCache({
    key: 'muirtl',
    stylisPlugins,
  });

  return (
    <CacheProvider value={cacheDir}>
      <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>
    </CacheProvider>
  );
};

export default ThemeProvider;
