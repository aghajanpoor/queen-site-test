import { Theme } from '@mui/material/styles';
import { darkTheme } from './dark';
import { lightTheme } from './light';

export function getThemeByName(theme: string): Theme {
  return themeMap[theme];
}

const themeMap: { [key: string]: Theme } = {
  darkTheme,
  lightTheme,
};
export const selectTheme: any[] = [
  {
    id: 'lightTheme',
    text: 'lightTheme',
  },
  {
    id: 'darkTheme',
    text: 'darkTheme',
  },
];

declare module '@mui/material/styles' {
  interface Theme {
    custom: {
      gray: React.CSSProperties['color'];
      lightGray: React.CSSProperties['color'];
    };
  }
  interface ThemeOptions {
    custom: {
      gray: React.CSSProperties['color'];
      lightGray: React.CSSProperties['color'];
    };
  }
}

