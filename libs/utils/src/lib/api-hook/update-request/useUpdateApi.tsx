import { useState, useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { SnackContext } from '../../../../../utils/src/lib/context/SnackContext';
import { environment } from 'apps/test-site/src/environments/environment';

const Base_Url = environment.baseUrl;

const useUpdateApi = (url: string) => {
  const [loading, setLoading] = useState<boolean>(false);
  const { showAlert } = useContext(SnackContext);
  const { t } = useTranslation(['common']);

  const updateApi = async (data: any) => {
    setLoading(true);

    const API_URL = `${Base_Url}${url}/${data.id}`;
    await fetch(`${API_URL}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then((res) => {
        if (res.ok) {
          showAlert({
            type: 'success',
            message: t('common:doneSuccessfully'),
          });
        }
      })
      .catch((error) => console.log(error))
      .finally(() => setLoading(false));
  };

  return { loading, updateApi };
};

export default useUpdateApi;
