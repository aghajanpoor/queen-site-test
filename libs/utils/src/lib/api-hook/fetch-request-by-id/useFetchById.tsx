import { UserSettingModel } from '@test-site/utils';
import { useState } from 'react';
import { environment } from 'apps/test-site/src/environments/environment';

const Base_Url = environment.baseUrl;

const useFetchById = (url: string) => {
  const [response, setResponse] = useState<UserSettingModel>();
  const [loading, setLoading] = useState<boolean>(true);

  const fetchById = async (id: number) => {
    setLoading(true);

    const API_URL = `${Base_Url}${url}/${id}`;
    await fetch(API_URL)
      .then((response) => response.json())
      .then((data) => {
        setLoading(false);
        setResponse(data);
      })
      .catch((e: any) => {
        setLoading(false);
        console.error(e);
      });
  };

  return { response, loading, fetchById };
};

export default useFetchById;
