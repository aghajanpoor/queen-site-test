import { useState, useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { SnackContext } from '../../../../../utils/src/lib/context/SnackContext';
import { environment } from 'apps/test-site/src/environments/environment';

const Base_Url = environment.baseUrl;

const useCreateApi = (url: string) => {
  const { t } = useTranslation(['common']);
  const [loading, setLoading] = useState<boolean>(false);
  const [refetchList, setRefetchList] = useState<boolean>(false);
  const { showAlert } = useContext(SnackContext);

  const callApi = async (data: any) => {
    setLoading(true);

    const API_URL = `${Base_Url}${url}`;
    await fetch(`${API_URL}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then((res) => {
        if (res.ok) {
          setRefetchList(true);
          showAlert({
            type: 'success',
            message: t('common:doneSuccessfully'),
          });
        }
      })
      .catch((error) => console.log(error))
      .finally(() => setLoading(false));
  };

  return { loading, callApi, refetchList };
};

export default useCreateApi;
