import { useContext, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { SnackContext } from '../../../../../utils/src/lib/context/SnackContext';
import { environment } from 'apps/test-site/src/environments/environment';

const Base_Url = environment.baseUrl;

const useDeleteApi = (url: string) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [deleted, setDeleted] = useState<boolean>(false);
  const { showAlert } = useContext(SnackContext);
  const { t } = useTranslation(['common']);

  const deleteApi = async (id: number) => {
    setLoading(true);

    const API_URL = `${Base_Url}${url}`;
    await fetch(`${API_URL}/${id}`, {
      method: 'DELETE',
    })
      .then((res) => res.json())
      .then((res) => {
        setDeleted(true);
        showAlert({
          type: 'success',
          message: t('common:doneSuccessfully'),
        });
      })
      .finally(() => setLoading(false));
  };

  return { loading, deleteApi, deleted };
};

export default useDeleteApi;
