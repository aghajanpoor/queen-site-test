import { useState, useEffect } from 'react';
import { environment } from 'apps/test-site/src/environments/environment';

const Base_Url = environment.baseUrl;

const useFetch = (url: string) => {
  const [response, setResponse] = useState<any[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [shouldRefetch, refetch] = useState({});

  const fetchData = async () => {
    setLoading(true);
    const API_URL = `${Base_Url}${url}`;
    await fetch(API_URL)
      .then((response) => response.json())
      .then((data) => {
        setResponse(data);
      })
      .catch((e: any) => {
        console.error(e);
      })
      .finally(() => setLoading(false));
  };

  useEffect(() => {
    fetchData();
  }, [shouldRefetch]);

  return { response, loading, refetch };
};

export default useFetch;
