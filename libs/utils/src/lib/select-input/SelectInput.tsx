import React from 'react';
import {
  Autocomplete,
  AutocompleteChangeReason,
  Box,
  TextField,
} from '@mui/material';
import { Controller, Control } from 'react-hook-form';
import Icons from '../icons/Icons';
import { useTranslation } from 'react-i18next';

interface IProps {
  name: string;
  options: any[];
  label?: string;
  placeholder?: string;
  control: Control<any>;
  error?: boolean;
  helpText?: any;
  defaultValue?: any;
  onChangeFunction?: (e: any) => void;
}

const SelectInput: React.FC<IProps> = ({
  options,
  label,
  placeholder,
  control,
  name,
  error,
  helpText,
  defaultValue,
  onChangeFunction,
}) => {
  const { t } = useTranslation(['user', 'common']);
  
  return (
    <Controller
      render={({ field }) => (
        <Autocomplete
          {...field}
          options={options}
          disableClearable
          getOptionLabel={(option) => t(`common:${option.text}`) ?? ' '}
          renderOption={(props, option) => (
            <Box
              component="li"
              {...props}
            >
              <Box
                sx={{
                  width: 30,
                  height: 30,
                  margin: "0 5px"
                }}
              >
                <Icons icon={option.icon} />
              </Box>
              {t(`common:${option.text}`)}
            </Box>
          )}
          onChange={(_, value, reason: AutocompleteChangeReason) => {
            field.onChange(value?.typeId);
            if (reason === 'clear') {
              field.onChange('');
            }
            if (onChangeFunction) {
              onChangeFunction(value);
            }
          }}
          value={options?.find((item) => item.typeId === field.value) || null}
          renderInput={(params) => (
            <>
              {label ? <label>{label}</label> : null}
              <TextField
                {...params}
                className={'outlined'}
                variant="outlined"
                size="small"
                placeholder={placeholder}
                fullWidth
                margin="dense"
                error={error}
                helperText={helpText}
              />
            </>
          )}
        />
      )}
      name={name}
      control={control}
      defaultValue={defaultValue}
    />
  );
};

export default SelectInput;
