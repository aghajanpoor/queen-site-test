import { TextField } from '@mui/material';
import React from 'react';
import { Controller, Control } from 'react-hook-form';

interface IProps {
  name: string;
  label?: string;
  placeholder?: string;
  type?: string;
  control: Control<any>;
  error?: boolean;
  helpText?: any;
  onChangeFunction?: (e: string) => void;
}

const TextInput: React.FC<IProps> = ({
  label,
  placeholder,
  type = 'text',
  control,
  name,
  error,
  helpText,
  onChangeFunction,
}) => {
  return (
    <Controller
      render={({ field }) => (
        <>
          {label && <label>{label}</label>}
          <TextField
            {...field}
            variant="outlined"
            type={type}
            onChange={(event: any) => {
              field.onChange(event.target.value);
              onChangeFunction && onChangeFunction(event.target.value);
            }}
            size="small"
            fullWidth
            margin="dense"
            label={placeholder}
            error={error}
            helperText={helpText}
          />
        </>
      )}
      name={name}
      control={control}
    />
  );
};

export default TextInput;
