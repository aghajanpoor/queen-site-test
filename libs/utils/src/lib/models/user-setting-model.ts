export interface UserSettingModel {
  id: number;
  typeId: string;
  text: string;
  linkUrl: string;
  icon: string;
}
