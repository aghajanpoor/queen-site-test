import styles from './loading.module.scss';

const Loading = () => {
  return (
    <div className={styles['loading-container']}>
      <div className={styles['lds_roller']}>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
};

export default Loading;
