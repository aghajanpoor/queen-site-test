import ThemeProvider from '../../../../libs/core/src/lib/components/themes/ThemeProvider';
import Layout from '../../../../libs/core/src/lib/components/layouts/Layout';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import Routes from '../../../../libs/core/src/lib/components/routes/routes';
import i18n from './i18n';
import { Suspense, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { selectLang } from '../../../../libs/core/src/lib/stores/langSlice';
import SnackContextProvider from '../../../../libs/utils/src/lib/context/SnackContext';

export function App() {
  const lang = useSelector(selectLang);

  useEffect(() => {
    i18n.changeLanguage(lang);
    document
      .getElementsByTagName('html')[0]
      .setAttribute('dir', lang === 'fa' ? 'rtl' : 'ltr');

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [lang]);

  return (
    <SnackContextProvider>
      <ThemeProvider>
        <BrowserRouter>
          <Suspense fallback>
            <Switch>
              <Route exact path="/">
                <Redirect to="/main/user-setting" />
              </Route>
              <Route path="/main">
                <Layout menuItems={Routes} />
              </Route>
            </Switch>
          </Suspense>
        </BrowserRouter>
      </ThemeProvider>
    </SnackContextProvider>
  );
}

export default App;
